package grupo2;

import java.util.Scanner;

public class Grupo2 {

    public static void main(String[] args) {
        String BI, sexo;
        int nascimento, nasDifTotal, SexoFTotal, taxaMTotal, n;
        float taxa, nasDifPercent, SexoFPercent, taxaMPercent;

        nasDifTotal = 0;
        SexoFTotal = 0;
        taxaMTotal = 0;
        n = 0;

        Scanner Read = new Scanner(System.in);

        do {
            System.out.println("Indique o BI");
            BI = Read.next();
            if ("0".equals(BI)) {
                break;
            }
            System.out.println("Indique o Ano de Nascimento");
            nascimento = Read.nextInt();
            System.out.println("Indique o sexo");
            sexo = Read.next();
            System.out.println("Indique a quantidade de álcool no sangue");
            taxa = Read.nextFloat();
            System.out.println("");

            int nasDif = 2022 - nascimento;

            if (nasDif < 30) {
                nasDifTotal++;
            }

            if ("f".equals(sexo)) {
                SexoFTotal++;
            } else {
                if (taxa > 0.5f) {
                    taxaMTotal++;
                }
            }

            n++;

        } while (!("0".equals(BI)));

        nasDifPercent = (float)nasDifTotal / n *100;
        SexoFPercent = (float)SexoFTotal / n *100;
        taxaMPercent = (float)taxaMTotal / n *100;

        System.out.println("Percentagem de menores de 30 anos: " + nasDifPercent);
        System.out.println("Percentagem do sexo femenino: " + SexoFPercent);
        System.out.println("Percentagem do sexo Maculino com taxa superior a 0.5g: " + taxaMPercent);

    }

}

package seguranca_social;

import java.util.Scanner;

/**
 *
 * @author jgsou
 */
public class Seguranca_Social {

    public static void main(String[] args) {
        float idade, salario, taxaEmpresa, taxaFunc, descEmp, descFunc, descTotal, salLiquido;

        idade = lerFloat("Introuduza a idade do funcionário");
        salario = lerFloat("Introuduza o salário do funcionário");
        if (idade < 55) {
            taxaEmpresa = 0.20f;
            taxaFunc = 0.17f;
        } else if (idade > 65) {
            taxaEmpresa = 0.5f;
            taxaFunc = 0.075f;
        } else {
            taxaEmpresa = 0.13f;
            taxaFunc = 0.13f;
        }

        descEmp = salario * taxaEmpresa;
        descFunc = salario * taxaFunc;
        descTotal = descEmp + descFunc;
        salLiquido = salario - descTotal;
        System.out.println("O desconto da empresa é: " + descEmp);
        System.out.println("O desconto do funcionário é " + descFunc);
        System.out.println("O desconto total é: " + descTotal);
        System.out.println("O valor a receber pelo funcionário é: " + salLiquido);

    }

    static float lerFloat(String pergunta) {
        float num;
        Scanner ler = new Scanner(System.in);
        System.out.println(pergunta);
        num = ler.nextFloat();
        return num;
    }

}
